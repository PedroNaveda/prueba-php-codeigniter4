<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/obtenerProducto/(:any)', 'Home::obtenerProducto/$1');
$routes->post('/crear', 'Home::crear');
$routes->get('/eliminar/(:any)', 'Home::eliminar/$1');
$routes->post('/actualizar', 'Home::actualizar');

