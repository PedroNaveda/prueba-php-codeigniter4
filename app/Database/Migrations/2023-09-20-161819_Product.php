<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Product extends Migration
{
   public function up()
    {
        $this->forge->addField([
            'idproduct' => [
                'type'           => 'INT',
                'constraint'     => 100,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '250',
            ],
            'reference' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'price' => [
                'type'           => 'INT',
            ],
            'idcategory' => [
                'type'           => 'INT',
            ],
            'stock' => [
                'type'           => 'INT',
            ],
            'createdate' => [
                'type'           => 'DATE',
            ],
        ]);
        $this->forge->addKey('idproduct', true);
        $this->forge->createTable('product');
    }

    public function down()
    {
        $this->forge->dropTable('product');
    }
}
