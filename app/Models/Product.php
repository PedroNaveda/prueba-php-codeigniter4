<?php namespace App\Models;

	use CodeIgniter\Model;

	/**
	  * Modelo para las operaciones sobre productos
	  */
	class Product extends Model {
 		public function listarProductos() {
 			$Productos = $this->db->query("SELECT * FROM product");

 			return $Productos->getResult();
 		}

 		public function insertar($datos){
 			$Productos = $this->db->table('product');
 			$Productos->insert($datos);

 			return $this->db->insertID();
 		}

 		public function obtenerProducto($idproduct) {
 			$Productos = $this->db->table('product');
 			$Productos->where($idproduct);
 			$Producto = $Productos->get()->getResultArray();

 			return  $Producto;
    	}

    	public function actualizar($datos, $idproduct){
 			$Productos = $this->db->table('product');
 			$Productos->set($datos);
 			$Productos->where('idproduct', $idproduct);

 			return $Productos->update();
 		}

 		public function eliminar($idproduct) {
 			$Producto = $this->db->table('product');
 			$Producto->where($idproduct);

 			return  $Producto->delete();
 		}

	}