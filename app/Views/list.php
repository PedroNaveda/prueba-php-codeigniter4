<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Prueba PHP</title>
  </head>
  <body>
    <div class="container">
      <h2 style="text-align: center;">Prueba PHP Codeigniter</h2>

      <div class="row">
        <div class="col-sm-12">
          <form method="POST" action="<?php echo base_url().'/crear' ?>">
            <label for="name">Nombre del Producto</label>
            <input type="text" name="name" id="name" required class="form-control">
            <label for="reference">Referencia</label>
            <input type="text" name="reference" id="reference" required class="form-control">
            <label for="price">Precio</label>
            <input type="number" name="price" id="price" required class="form-control">
            <label for="idcategory">Categoria</label>
            <input type="text" name="idcategory" id="idcategory" required class="form-control">
            <label for="stock">Stock</label>
            <input type="number" name="stock" id="stock" required class="form-control">
            <label for="createdate">Fecha de Creacion</label>
            <input type="date" name="createdate" id="createdate" required class="form-control">
            <br>
            <button class="btn btn-warning"> Guardar </button>
          </form>
        </div>
      </div>
      <hr>
      <h2 style="text-align: center;">Lista de Productos</h2>
      <div class="row">
        <div class="col-sm-12">
          <div class="table table-responsive">
            <table class="table table-hover table-bordered">
              <tr>
                <th>Nombre del Producto</th>
                <th>Referencia</th>
                <th>Precio</th>
                <th>Categoria</th>
                <th>Stock</th>
                <th>Fecha de Creacion</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
              <?php foreach ($datos as $producto):?>
                <tr>
                <td><?php echo $producto->name ?></td>
                <td><?php echo $producto->reference ?></td>
                <td><?php echo $producto->price ?></td>
                <td><?php echo $producto->category ?></td>
                <td><?php echo $producto->stock ?></td>
                <td><?php echo $producto->createdate ?></td>
                <td>
                    <a href="<?php echo base_url().'/obtenerProducto/'.$producto->idproduct ?>" class="btn btn-warning btn-sm">Editar</a>
                </td>
                <td>
                    <a href="<?php echo base_url().'/eliminar/'.$producto->idproduct ?>" class="btn btn-danger btn-sm">Eliminar</a>
                </td>
              </tr>
              <?php endforeach; ?>
            </table>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        let mensaje = '<?php echo $mensaje ?>';

        if (mensaje == '1') {
            swal('Agregado con exito!','success');
        } else if (mensaje == '0'){
            swal('Fallo al agregar!','error');
        } else if (mensaje == '2'){
            swal('Actualizado con exito!','success');
        } else if (mensaje == '3'){
            swal('Fallo al Actualizar!','error');
        } else if (mensaje == '4'){
            swal('Eliminado con exito!','success');
        } else if (mensaje == '5'){
            swal('Fallo al eliminar!','error');
        }
    </script>
</body>
</html>