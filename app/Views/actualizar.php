<?php 

    $idproduct  = $datos[0]['idproduct'];
    $name       = $datos[0]['name'];
    $reference  = $datos[0]['reference'];
    $price      = $datos[0]['price'];
    $category   = $datos[0]['category'];
    $stock      = $datos[0]['stock'];
    $createdate = $datos[0]['createdate']; 

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Actualizar</title>
  </head>
  <body>
    <div class="container">
      <h1>Prueba PHP Codeigniter</h1>
      <div class="row">
        <div class="col-sm-12">
          <?php foreach ($datos as $producto):?>
          <form method="POST" action="<?php echo base_url().'/actualizar' ?>">
            <input type="text" id="idproduct" name="idproduct" hidden="" value="<?php echo $idproduct ?>">
            <label for="name">Nombre del Producto</label>
            <input type="text" name="name" id="name" required class="form-control" 
                    value="<?php echo $name ?>">
            <label for="reference">Referencia</label>
            <input type="text" name="reference" id="reference" required class="form-control" 
                    value="<?php echo $reference ?>">
            <label for="price">Precio</label>
            <input type="number" name="price" id="price" required class="form-control" 
                    value="<?php echo $price ?>">
            <label for="category">Categoria</label>
            <input type="text" name="category" id="category" required class="form-control" 
                    value="<?php echo $category ?>">
            <label for="stock">Stock</label>
            <input type="number" name="stock" id="stock" required class="form-control" 
                    value="<?php echo $stock ?>">
            <label for="createdate">Fecha de Creacion</label>
            <input type="date" name="createdate" id="createdate" required class="form-control" 
                    value="<?php echo $createdate ?>">
            <br>
            <button class="btn btn-warning"> Guardar </button>
          </form>
          <?php endforeach; ?>
        </div>
      </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>