<?php
namespace App\Controllers;

use App\Models\Product;

class Home extends BaseController
{
    public function index(): string {
        $Product = new Product();
        $datos = $Product->listarProductos();

        $mensaje = session('mensaje');

        if ($datos) {
            $data = [
                        "datos" => $datos,
                        "mensaje" => $mensaje
                    ];
        } else {
            $data = "No Existen Productos";
        }

        return view('list', $data);
    }

    public function crear() {

        if ($_POST['name'] && $_POST['reference'] && $_POST['price'] && $_POST['category'] && $_POST['stock'] && $_POST['createdate']) {
            $datos = [
                        "name" => $_POST['name'], 
                        "reference" => $_POST['reference'],
                        "price" => $_POST['price'],
                        "category" => $_POST['category'],
                        "stock" => $_POST['stock'],
                        "createdate" => $_POST['createdate'],
                    ];
            $Product = new Product();
            $NewProduct = $Product->insertar($datos);

            if ($NewProduct > 0){
                return redirect()->to(base_url().'/')->with('mensaje', '1');
            }
            else{
                return redirect()->to(base_url().'/')->with('mensaje', '0');
            }

        } else {
            return redirect()->to(base_url().'/')->with('mensaje', '0');
        }

    }

    public function actualizar() {

        if ($_POST['idproduct'] && $_POST['name'] && $_POST['reference'] && $_POST['price'] && $_POST['category'] && $_POST['stock'] && $_POST['createdate']) {
            $datos = [
                        "name" => $_POST['name'], 
                        "reference" => $_POST['reference'],
                        "price" => $_POST['price'],
                        "category" => $_POST['category'],
                        "stock" => $_POST['stock'],
                        "createdate" => $_POST['createdate'],
                    ];
            $idproduct = $_POST['idproduct'];
            $Product = new Product();
            $UpProduct = $Product->actualizar($datos, $idproduct);

            if ($UpProduct){
                return redirect()->to(base_url().'/')->with('mensaje', '2');
            }
            else{
                return redirect()->to(base_url().'/')->with('mensaje', '3');
            }

        } else {
            return redirect()->to(base_url().'/')->with('mensaje', '3');
        }

    }

    public function obtenerProducto($idproduct) {
        $data = ["idproduct" => $idproduct];
        $Product = new Product();
        $respuesta = $Product->obtenerProducto($data);

        $datos = ["datos" => $respuesta];

        return view('actualizar', $datos);
    }
    public function eliminar($idproduct) {
        $data = ["idproduct" => $idproduct];
        $Product = new Product();
        $respuesta = $Product->eliminar($data);

        if ($respuesta){
                return redirect()->to(base_url().'/')->with('mensaje', '4');
            }
            else{
                return redirect()->to(base_url().'/')->with('mensaje', '5');
            }
    }

}
